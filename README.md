# Contador de Archivos y Directorios

  

# Introducción

Con el fin de introducir y profundizar el uso de bash la cátedra de Sistemas Operativos y Redes propuso la realización de un script que al pasarle un directorio devuelva la cantidad de directorio, archivos regulares y no regulares.

Este informe contiene el enunciado, la interpretación del mismo, un pseudocódigo, un modo de uso y las conclusiones.

Consideramos que dado que el uso de bash es imprescindible cuando uno utiliza un sistema operativo en base linux, esta tarea resulta útil para mantener un script que logre esto, ya que no existe un comando que haga esto.

  
  

# Enunciado
Escriba un script que al pasarle un directorio en la línea de comando realice las siguientes tareas:

1. Verificar que el argumento pasado en la línea de comandos es efectivamente un directorio.
2. Revisar el contenido del directorio e imprimir:
	1. Cantidad de directorios que contiene.
	2. Cantidad de archivos regulares que contiene.
	3. Cantidad de archivos que no son regulares ni son directorios que contiene


# Modo de Uso
Para utilizar el script, se debe ejecutar desde la línea de comandos de la siguiente manera:

    ./Ej10-P1.sh /ruta/al/directorio

Asegúrese de proporcionar la ruta completa al directorio que desea analizar, de lo contrario generará un error y terminará el script.
