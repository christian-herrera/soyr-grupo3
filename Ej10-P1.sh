#!/bin/bash
directorio=$1

if [[ -d $directorio ]];
then
	dir=$(ls -la $directorio | grep '^d' | wc -l)
	reg=$(ls -la $directorio | grep '^-' | wc -l)
	noreg=$(ls -la $directorio | grep -vE '^(d|-)' | wc -l)

	printf "Cant. Directorios:\t\t$dir\n"
	printf "Cant. de Arch. Regulares:\t$reg\n"
	printf "Cant. de Arch. No Regulares:\t$((noreg-1))\n"

else
	printf "ERROR:\tEl archivo no es un directorio\n"
	exit
fi

